double S1DTCorr(double S1, double DT) {
  TF1* f1 = new TF1("f1", "pol3", 0, 800000);
  f1->SetParameters(0, 0.04003, -5.673E-8, 6.53E-14);
  double firstCorr = S1*(S1+f1->Eval(400000))/(S1+f1->Eval(DT));

  TF1* f2 = new TF1("f2", "pol2", 0, 800000);
  f2->SetParameters(0, -0.005799, 1.416E-8);
  return firstCorr*(firstCorr+f2->Eval(400000))/(firstCorr+f2->Eval(DT));
}

double S1TBACorr(double S1, double TBA) {
  TF1* f1 = new TF1("f1", "pol3", -0.8, 0.2);
  f1->SetParameters(0, -3.273E4, -6.207E4, -7.45E4);
  double firstCorr =  S1*(S1+f1->Eval(-0.4))/(S1+f1->Eval(TBA));

  TF1* f2 = new TF1("f2", "pol2", -0.8, 0.2);
  f2->SetParameters(0, 5594, 1.244E4);
  return firstCorr*(firstCorr+f2->Eval(-0.4))/(firstCorr + f2->Eval(TBA));
}
